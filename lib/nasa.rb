require_relative 'planet'
require_relative 'rover'

class Nasa

  attr_reader :missions

  def initialize
    @missions = Array.new
    @mission_code = {
      'N'    => :north,
      'S'    => :south,
      'E'    => :east,
      'W'    => :west,
      'L'    => :left,
      'R'    => :right,
      'M'    => :move,
      :north => { step_x:  0, step_y: +1 },
      :south => { step_x:  0, step_y: -1 },
      :east  => { step_x: +1, step_y:  0 },
      :west  => { step_x: -1, step_y:  0 }
    }
  end

  def add_mission file
    raise "Empty file" if File.zero? file
    mission = Array.new
    File.open(file, 'r') do |file|
      mission << parse_planet(file.gets)
      file.each_line do |line|
        mission << parse_rover(line)
        mission << parse_commands(file.gets)
      end
    end
    @missions << mission
  end

  def execute_missions
    return false if @missions.empty?
    @missions.each do |mission|
      planet = mission.first
      for i in (1...mission.length).step(2)
        rover = mission[i]
        commands = mission[i+1]
        begin
          planet.place_rover rover
          start_mission planet, rover, commands
          puts "#{rover.x} #{rover.y} #{rover.direction}"
        rescue Exception => e
          puts e.message
          puts 'Could not complete the mission'
        end
      end
    end
    true
  end

  def start_mission planet, rover, commands
    commands.each do |c|
      if c == :move
        direction_properties = @mission_code[rover.direction]
        if planet.free_site? rover.x + direction_properties[:step_x], rover.y + direction_properties[:step_y]
          rover.command c
        else
          puts "Rover collision detected"
          puts "Rover will not move"
        end
      else
        rover.command c
      end
    end
  end

  def parse_planet limits
    planet_x_limit, planet_y_limit = limits.split ' '
    planet_x_limit = planet_x_limit.to_i
    planet_y_limit = planet_y_limit.to_i
    raise "Invalid file" if planet_x_limit == 0 and planet_y_limit == 0
    Planet.new planet_x_limit, planet_y_limit
  end

  def parse_rover rover_data
    rover_x, rover_y, rover_direction = rover_data.split ' '
    raise "Invalid file" unless @mission_code.key? rover_direction
    rover = Rover.new rover_x.to_i, rover_y.to_i, @mission_code[rover_direction]
  end

  def parse_commands commands
    commands = commands.strip!.split('')
    commands.map! do |c|
      raise "Invalid file" unless @mission_code.key? c
      @mission_code[c]
    end
  end

  private :start_mission, :parse_planet, :parse_rover, :parse_commands

end
