
class Planet

  attr_reader :x_limit, :y_limit

  def initialize x_limit, y_limit
    raise "Invalid x value" unless x_limit.kind_of? Numeric
    raise "Invalid y value" unless y_limit.kind_of? Numeric
    raise "Negative x value" if x_limit < 0
    raise "Negative y value" if y_limit < 0
    @x_limit = x_limit
    @y_limit = y_limit
    @rovers = Array.new
  end

  def place_rover rover
    raise "Not an instance of Rover" unless rover.instance_of? Rover
    raise "Rover in the same site" unless free_site? rover.x, rover.y
    raise "x outside of bounds" if rover.x > x_limit
    raise "y outside of bounds" if rover.y > y_limit
    @rovers << rover
  end

  def free_site? x, y
    raise "Invalid x value" unless x.kind_of? Numeric
    raise "Invalid y value" unless y.kind_of? Numeric
    raise "Negative x value" if x < 0
    raise "Negative y value" if y < 0
    raise "x outside of bounds" if x > x_limit
    raise "y outside of bounds" if y > y_limit
    return true if @rovers.empty?
    @rovers.each do |r|
      if r.x == x and r.y == y
        return false
      end
    end
    true
  end

end
