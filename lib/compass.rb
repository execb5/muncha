
class Compass

  def initialize facing
    @cardinal_points = [:north, :east, :south, :west]
    raise "Invalid facing direction" unless @cardinal_points.include? facing
    @cardinal_points.rotate! while @cardinal_points.first != facing
  end

  def direction
    @cardinal_points.first
  end

  def update direction
    return false unless [:left, :right].include? direction
    direction == :left ? @cardinal_points.rotate!(-1) : @cardinal_points.rotate!
    true
  end

end

