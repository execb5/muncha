require_relative 'compass'

class Rover

  attr_reader :x, :y

  def initialize x, y, direction
    raise "Invalid x value" unless x.kind_of? Numeric
    raise "Invalid y value" unless y.kind_of? Numeric
    raise "Invalid facing direction value" unless [:north, :east, :south, :west].include? direction
    @x = x
    @y = y
    @compass = Compass.new direction
  end

  def direction
    @compass.direction
  end

  def command instruction
    raise "Invalid command" unless [:move, :right, :left].include? instruction
    move instruction
  end

  def move instruction
    if instruction == :move
      @y += 1 if direction == :north
      @y -= 1 if direction == :south
      @x += 1 if direction == :east
      @x -= 1 if direction == :west
    else
      @compass.update instruction
    end
  end

  private :move

end

