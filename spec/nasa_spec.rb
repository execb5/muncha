require 'nasa'

describe Nasa do

  describe "#add_mission" do

    context "given a empty file" do
      it "will raise an error" do
        nasa = Nasa.new
        expect { nasa.add_mission "inputs/empty_file" }.to raise_error "Empty file"
      end
    end

    context "given an invalid file" do
      it "will raise an error" do
        nasa = Nasa.new
        expect { nasa.add_mission "inputs/invalid_file" }.to raise_error "Invalid file"
      end
    end

    context "given a valid mission" do
      it "will have onde more mission" do
        nasa = Nasa.new
        number_of_missions = nasa.missions.length
        nasa.add_mission "inputs/test_real"
        expect(nasa.missions.length).to eq(number_of_missions + 1)
      end
    end

  end

  describe "#execute_missions" do

    context "given 0 missions" do
      it "will return false" do
        nasa = Nasa.new
        expect(nasa.execute_missions).to be false
      end
    end

    context "given at least 1 mission" do
      it "will return true" do
        nasa = Nasa.new
        nasa.add_mission "inputs/test_real"
        expect(nasa.execute_missions).to be true
      end
    end

  end

end
