require 'planet'

describe Planet do

  describe "#new" do

    context "given negative x as a x limit" do
      it "wont be a valid planet" do
        expect { Planet.new -1, 5 }.to raise_error "Negative x value"
      end
    end

    context "given negative y as a y limit" do
      it "wont be a valid planet" do
        expect { Planet.new 5, -1 }.to raise_error "Negative y value"
      end
    end

    context "given positive numbers as x and y" do
      it "should be a valid planet" do
        mars = Planet.new 5, 5
        expect(mars).to be_an_instance_of Planet
        expect(mars.x_limit).to eq(5)
        expect(mars.y_limit).to eq(5)
      end
    end

    context "given anything but numbers as x" do
      it "will raise an error" do
        expect { Planet.new "bla", 5 }.to raise_error "Invalid x value"
      end
    end

    context "given anything but numbers as y" do
      it "will raise an error" do
        expect { Planet.new 5, "bla" }.to raise_error "Invalid y value"
      end
    end

  end

  describe "#place_rover" do

    context "given anything but a rover" do
      it "will raise an error" do
        mars = Planet.new 5, 5
        expect { mars.place_rover(:rover) }.to raise_error "Not an instance of Rover"
      end
    end

    context "given there is already a rover in the same site" do
      it "will raise an error" do
        mars = Planet.new 5, 5
        mars.place_rover(Rover.new(3, 3, :north))
        expect { mars.place_rover(Rover.new(3, 3, :west)) }.to raise_error "Rover in the same site"
      end
    end

    context "given an outside of bounds x" do
      it "will raise an error" do
        mars = Planet.new 3,3
        expect { mars.place_rover(Rover.new(4,2, :west)) }.to raise_error "x outside of bounds"
      end
    end

    context "given an outside of bounds y" do
      it "will raise an error" do
        mars = Planet.new 3,3
        expect { mars.place_rover(Rover.new(2,4, :west)) }.to raise_error "y outside of bounds"
      end
    end

  end

  describe "#free_site?" do

    context "given an outside of bounds x" do
      it "will raise an error" do
        mars = Planet.new 3,3
        expect { mars.free_site? 4, 2 }.to raise_error "x outside of bounds"
      end
    end

    context "given an outside of bounds y" do
      it "will raise an error" do
        mars = Planet.new 3,3
        expect { mars.free_site? 2, 4 }.to raise_error "y outside of bounds"
      end
    end

    context "given negative x" do
      it "will raise an error" do
        mars = Planet.new 5, 5
        expect { mars.free_site? -1, 3 }.to raise_error "Negative x value"
      end
    end

    context "given negative y" do
      it "will raise an error" do
        mars = Planet.new 5, 5
        expect { mars.free_site? 3, -1 }.to raise_error "Negative y value"
      end
    end

    context "given positive numbers as x and y and no rover is in place" do
      it "should return true" do
        mars = Planet.new 5, 5
        expect(mars.free_site? 3, 3).to be true
      end
    end

    context "given positive numbers as x and y and a rover is in place" do
      it "should return false" do
        mars = Planet.new 5, 5
        mars.place_rover(Rover.new(3, 3, :west))
        expect(mars.free_site? 3, 3).to be false
      end
    end

    context "given anything but numbers as x" do
      it "will raise an error" do
        mars = Planet.new 5, 5
        expect { mars.free_site? :tres, -1 }.to raise_error "Invalid x value"
      end
    end

    context "given anything but numbers as y" do
      it "will raise an error" do
        mars = Planet.new 5, 5
        expect { mars.free_site? 3, :tres }.to raise_error "Invalid y value"
      end
    end

  end

end
