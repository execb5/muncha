require 'rover'

describe Rover do

  describe "#new" do

    it "should be a valid rover" do
      rover = Rover.new 1, 2, :north
      expect(rover).to be_an_instance_of Rover
      expect(rover.x).to eq(1)
      expect(rover.y).to eq(2)
      expect(rover.direction).to eq(:north)
    end

    context "given x is not a number" do
      it "will not be a valid rover" do
        expect { Rover.new '1', 2, :north }.to raise_error "Invalid x value"
        expect { Rover.new  :n, 2, :north }.to raise_error "Invalid x value"
        expect { Rover.new ' ', 2, :north }.to raise_error "Invalid x value"
      end
    end

    context "given y is not a number" do
      it "will not be a valid rover" do
        expect { Rover.new 1, '2', :north }.to raise_error "Invalid y value"
        expect { Rover.new 1,  :n, :north }.to raise_error "Invalid y value"
        expect { Rover.new 1, ' ', :north }.to raise_error "Invalid y value"
      end
    end

    context "given facing is not a valid direction" do
      it "will not be a valid rover" do
        expect { Rover.new 1, 2, :left }.to raise_error "Invalid facing direction value"
        expect { Rover.new 1, 2, "blal" }.to raise_error "Invalid facing direction value"
        expect { Rover.new 1, 2, 3 }.to raise_error "Invalid facing direction value"
      end
    end

  end

  describe "#command" do

    context "given an invalid command" do
      it "will raise an error" do
        rover = Rover.new 1, 2, :north
        expect{rover.command(:bla)}.to raise_error "Invalid command"
      end
    end

    context "given LEFT when facing WEST" do
      it "will face SOUTH" do
        rover = Rover.new 1, 2, :west
        rover.command :left
        expect(rover.direction).to eq(:south)
      end
    end

    context "given RIGHT when facing WEST" do
      it "will face NORTH" do
        rover = Rover.new 1, 2, :west
        rover.command :right
        expect(rover.direction).to eq(:north)
      end
    end

    context "given MOVE when NORTH, x is 2 and y is 2" do
      it "will only change the y value, by plus one" do
        rover = Rover.new 2, 2, :north
        old_y = rover.y
        rover.command :move
        expect(rover.y).to eq(old_y + 1)
      end
    end

    context "given MOVE when SOUTH, x is 2 and y is 2" do
      it "will only change the y value, by minus one" do
        rover = Rover.new 2, 2, :south
        old_y = rover.y
        rover.command :move
        expect(rover.y).to eq(old_y - 1)
      end
    end

    context "given MOVE when EAST, x is 2 and y is 2" do
      it "will only change the x value, by plus one" do
        rover = Rover.new 2, 2, :east
        old_x = rover.x
        rover.command :move
        expect(rover.x).to eq(old_x + 1)
      end
    end

    context "given MOVE when WEST, x is 2 and y is 2" do
      it "will only change the x value, by minus one" do
        rover = Rover.new 2, 2, :west
        old_x = rover.x
        rover.command :move
        expect(rover.x).to eq(old_x - 1)
      end
    end

  end

  describe "#direction" do

    context "given :north when instantiating a new rover" do
      it "will return :north when direction is asked" do
        rover = Rover.new 2, 2, :north
        expect(rover.direction).to eq(:north)
      end
    end

  end

end
