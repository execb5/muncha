require 'compass'

describe Compass do

  describe "#new" do

    it "should be a valid compass" do
      compass = Compass.new :west
      expect(compass).to be_an_instance_of Compass
    end

    context "given WEST" do
      it "direction will be WEST" do
        compass = Compass.new :west
        expect(compass.direction).to eq(:west)
      end
    end

    context "given anything but a facing direction" do
      it "will raise an exception" do
        expect { Compass.new 'bla' }.to raise_error "Invalid facing direction"
        expect { Compass.new :blal }.to raise_error "Invalid facing direction"
        expect { Compass.new "tes" }.to raise_error "Invalid facing direction"
        expect { Compass.new 10000 }.to raise_error "Invalid facing direction"
      end
    end

  end

  describe "#direction" do
    it "should not be nil" do
      compass = Compass.new :south
      expect(compass.direction).not_to eql(nil)
    end
  end

  describe "#update" do

    context "given left when south" do
      it "returns true" do
        compass = Compass.new :south
        expect(compass.update(:left)).to be true
      end
    end

    context "given anything but a direction" do
      it "return false" do
        compass = Compass.new :south
        expect(compass.update(true)).to be false
        expect(compass.update('  ')).to be false
        expect(compass.update(:not_command)).to be false
      end
    end

  end

end
